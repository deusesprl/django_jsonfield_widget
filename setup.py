from setuptools import find_packages, setup
from django_jsonfield_widget import __version__
from os import path

here = path.abspath(path.dirname(__file__))
# Get the long description from the relevant file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='django_jsonfield_widget',
    version=__version__,
    url='https://bitbucket.org/deusesprl/django_jsonfield_widget',
    author='Maxime Deuse',
    author_email='m.deuse@deuse.be',
    description='A widget for the Django JSONFIELD, this widget usefulness is to have dynamic fields for a given model.',
    long_description=long_description,  # this is the
    license='MIT',
    zip_safe=False,
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'django>=1.11.0',
    ],
)
