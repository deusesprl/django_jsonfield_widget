Django JsonField widgets
************************
Installation
++++++++++++
Install the package with `pip install django_jsonfield_widget`

Then add it to the `INSTALLED_APPS` in `settings.py` :

.. code-block:: python

    INSTALLED_APPS = [
        '...',

        'django_jsonfield_widget',

        '...'
    ]


JsonFieldSchemaWidget
+++++++++++++++++++++
The JsonFieldSchemaWidget is a widget that adds a fixed schema to the json. The Json is blocked in this canvas and cannot be adapted by the enduser when this widget is used. This widget was developped to create dynamic form schemas where the dynamic fields are controlled by python code

To use the JsonFieldSchemaWidget in a form do:

.. code-block:: python

    from django_jsonfield_widget.widgets import JsonFieldSchemaWidget
    from collections import OrderedDict

    class SomeForm(forms.ModelForm):
        class Meta:
            model = SomeModel
            fields = '__all__'
            widgets = {
                'json_field': JsonFieldSchemaWidget(schema=OrderedDict({
                    'height': {
                        'verbose_name': 'What is your height in cm?',
                        'type': 'IntegerField'
                    },
                    'name': {
                        'verbose_name': 'What is your name?',
                        'type': 'CharField',
                    },
                    'has_freckless': {
                        'verbose_name': 'Do you have freckless?',
                        'type': 'BooleanField',
                    }
                })),
            }


.. figure:: examples/jsonfield_schema_widget.png
    :alt: jsonfield schema widget example

    JsonFieldSchemaWidget layout for the above settings.


About
-----

developped by : `DEUSE - Développement Software <https://www.deuse.be/fr/services/developpement-logiciel/>`_




    