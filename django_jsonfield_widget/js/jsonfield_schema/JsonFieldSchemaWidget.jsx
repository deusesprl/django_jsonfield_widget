'use strict'

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { render } from 'react-dom'
import _ from 'lodash'
import u from 'updeep'

import { integrator } from '../django_react_integration'


const propTypes = {
    json: PropTypes.object.isRequired,
    schema: PropTypes.object.isRequired,
    field_name: PropTypes.string.isRequired,
}

const defaultProps = {}


class JsonFieldSchemaWidget extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: props.json,
        }
    }

    setField = (key, value) => {
        // We set the value of the fields. We use updeep to change the object partially.
        let obj = {}
        obj[key] = value

        this.setState(u({
            data: obj
        }, this.state))
    }

    renderExtraField = (key, i) => {
        // we render the extra fields one by one.
        let value = _.get(this.state.data, key, '')
        let field = _.get(this.props.schema, key, null)
        let fieldName = field.verbose_name
        let fieldType = field.type

        return (
            <div key={key}>
                {i != 0 && <hr style={{ marginTop: '10px', marginBottom: '10px'}}/>}

                <label>{fieldName}</label>
                {this.renderInput(fieldType, key, value)}
            </div>
        )
    }

    renderInput = (fieldType, key, value) => {
        // the render input depends mainly on the type of value we saved in the system. The type of values
        // are stored in the schema given as props.
        let onChange = e => this.setField(key, e.target.value)
        let onChangeCheckbox = e => this.setField(key, e.target.checked)

        switch (fieldType) {
            case 'IntegerField':
                return (
                    <input
                        type="number"
                        step="1"
                        value={value}
                        onChange={onChange}
                    />
                )
            case 'FloatField':
                return (
                    <input
                        type="number"
                        value={value}
                        onChange={onChange}
                    />
                )
            case 'BooleanField':
                return (
                    <input
                        type="checkbox"
                        checked={value}
                        onChange={onChangeCheckbox}
                    />
                )
            case 'CharField':
            default:
                return (
                    <input
                        type="text"
                        value={value}
                        onChange={onChange}
                    />
                )
        }
    }

    render() {
        /*
         * The textarea is not displayed but it is the field that will send the data back to our django form.
         * The other fields are only there to have a beautifull render.
         */
        return (
            <div>
                <textarea
                    name={this.props.field_name}
                    id={`id_${this.props.field_name}`}
                    style={{ display: 'None' }}
                    value={JSON.stringify(this.state.data)}
                    readOnly={true}/>

                {Object.keys(this.props.schema).map((key, i) => this.renderExtraField(key, i))}
            </div>
        )
    }
}


// we integrate the json react fields in the place of our django field.
integrator('react_jsonfield_schema_widget', JsonFieldSchemaWidget)
