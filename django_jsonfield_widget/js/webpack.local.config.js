var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
var u = require('updeep')

var config = require('./webpack.config.js');

config.output.publicPath = 'http://0.0.0.0:8084/static/bundles/';

module.exports = config
