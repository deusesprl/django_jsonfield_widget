'use strict'

import React from 'react'
import { render } from 'react-dom'

export const integrator = (classname, react_component) => {
    /*
    *  The purpose of this function is to be an integrator between React and Django.
    *  The problem this is supposed to solve is the following: how can we integrate
    *  <ReactComponent props1={some_variable1} props2={2} /> into a Django app?
    *
    *  The best possible integration we found is to provide something like this in django
    *  <div class='react-component' data-props1={{ some_variable1 }} data-props2="2"></div>
    *  and then have a javascript script that translates this once the page has loaded to whatever
    *  we want it to be.
    *
    *  This script is represented in this function, by calling integrator(classname, react_component)
    *  at the end of the html template we render the react component where the div with these classes was.
    */
    let items = document.getElementsByClassName(classname)

    Array.from(items).forEach((element) => {
        let data_attributes = element.dataset
        let class_names = element.className

        // we transform the data attributes from a json value to an object with the right types and values.
        let props = {}
        for (let data_attribute in data_attributes) {
            let json = data_attributes[data_attribute]
            props[data_attribute] = JSON.parse(json)
        }
        // we append the class of the parent to the component
        props['class']=class_names

        // Finally, we render the component with its props in the right html element.
        render(React.createElement(react_component, props), element);
    })
}

export default integrator
