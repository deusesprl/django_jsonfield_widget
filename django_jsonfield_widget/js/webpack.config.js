var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

var __destinationdirname = '../static/admin/js/django_hstore/bundles/'

var config = {
    entry: {
        JsonFieldSchemaWidget: './jsonfield_schema/JsonFieldSchemaWidget.jsx',
    },
    output: {
        path: path.join(__dirname, "../static/admin/js/django_jsonfield_widget/bundles"),
        filename: "[name].js"
    },
    devServer: {
        contentBase: ".",
        host: "localhost",
        port: 8084
    },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'stage-0', 'react']
                }
            },
            // Needed for the css-loader when [bootstrap-webpack](https://github.com/bline/bootstrap-webpack)
            // loads bootstrap's css.
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader','css-loader')
            },
            {
                test: /\.png.*$/,
                loaders: ['url-loader?limit=100000&mimetype=image/png'],
                exclude: /node_modules/
            },
            {
                test: /\.less$/,
                    loader: "style!css!less"
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")
            },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
            { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }
        ],

    },
    modulesDirectories: ['node_modules'],
    plugins: [
        new ExtractTextPlugin("[name].css"),
        new BundleTracker({filename: './webpack-stats.json'})
    ],
    resolve: {
        extensions: ['', '.js', '.jsx'],
        root: [
            path.resolve('./'),
        ]
    }
}

module.exports = config
