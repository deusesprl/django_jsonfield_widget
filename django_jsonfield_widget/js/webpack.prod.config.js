var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var config = require('./webpack.config.js');

config.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
        drop_console: true,
    }
}))

config.output.publicPath = '/static/bundles/';

config.plugins.push(new webpack.DefinePlugin({
      'process.env':{
            'NODE_ENV': JSON.stringify('production')
      }
}))

module.exports = config
